const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [] mongoDB connection -- mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.ldoiqaf.mongodb.net/B217_to_do?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We are connected to the cloud database"));

// [] Mongoose schemas - det. stucture of our docs to be stored cloud database - acts as our data blueprint and guide

const taskSchema = new mongoose.Schema({
    name : {
        type: String,
        required: [true, "Tasks name is required"]
    },
    status : {
        type: String,
        default: "pending"
    }
});

const userSchema = new mongoose.Schema({
    username : {
        type: String,
        required: [true, "Username is required"]
    },
    password : {
        type: String,
        required: [true, "Password is required"]
    }
});

// [] Models 
const Task = mongoose.model("Task", taskSchema);
const User = mongoose.model("User", userSchema);

// [] creation of to do list routes

app.use (express.json());
app.use(express.urlencoded({extended:true}));

app.post("/tasks", (req, res) => {
    Task.findOne({name: req.body.name}, (err, result) => {
        if(result !== null && result.name == req.body.name ){
            return res.send("Duplicate found");
        }else{
            let newTask = new Task({
                name: req.body.name
            });

            newTask.save((saveErr, saveTask) => {
                if(saveErr == true){
                    return console.error(saveErr);
                }else{
                    return res.status(201).send("New Task created.")
                }
            })

        }
    })
})

app.use (express.json());
app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username}, (err, result) => {
        if(result !== null && result.username == req.body.username ){
            return res.send("Duplicate found");
        }else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });

            newUser.save((saveErr, saveUser) => {
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return res.status(201).send("New user registered")
                }
            })

        }
    })
})




// GET Method
app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        if(err){
            return console.log(err);
        }else{
            return res.status(200).json({
                data : result
            })
        }
    })
})

app.get("/signup", (req, res) => {
    User.find({}, (err, result) => {
        if(err){
            return console.log(err);
        }else{
            return res.status(200).json({
                data : result
            })
        }
    })
})




app.listen(port, () => console.log(`Server running at port ${port}`))
